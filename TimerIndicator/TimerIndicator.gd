extends RichTextLabel


var ms = 0
var s = 0
var m = 0

var str_ms: String
var str_s: String
var str_m: String

func _ready():
	m = TimeHolder.m
	s = TimeHolder.s
	ms = TimeHolder.ms

func _process(delta):
	TimeHolder.m = m
	TimeHolder.s = s
	TimeHolder.ms = ms
	if ms < 10:
		str_ms = "0" + str(ms)
	else: str_ms = str(ms)
	if s < 10:
		str_s = "0" + str(s)
	else: str_s = str(s)
	if m < 10:
		str_m = "0" + str(m)
	else: str_m = str(m)
	if ms > 59:
		s += 1
		ms = 0
	if s > 59:
		m += 1
		s = 0

	set_text(str_m + ':' + str_s + ':' + str_ms)

func _on_Timer_timeout():
	ms += 1
