extends Node

var current_level = 0

func load_next_level():
	Sigleton.current_level += 1
	get_tree().change_scene_to(load(Sigleton.worlds[Sigleton.current_level]))


var worlds = [
	"res://MainMenu/MainMenu.tscn",
	"res://Worlds/World1.tscn",
	"res://Worlds/World2.tscn",
	"res://Worlds/World3.tscn",
	"res://Worlds/World4.tscn",
	"res://Worlds/World5.tscn",
	"res://Worlds/World6.tscn"
]
