extends KinematicBody2D

var motion: Vector2
var collision
const move_speed = 190
var alive = true
var start_sound_played = false

func _ready():
	$AnimationPlayer.play("idle")
	Global.connect("dead", self, "die")


func _process(delta):

	if Input.is_action_just_pressed("ui_accept"):
		if alive == false:
			Global.game_start()
			alive = true
			get_tree().reload_current_scene()

	if Input.is_action_just_pressed("ui_right") and position.x != 570 and alive == true:
		motion.x += move_speed
		$Sprite.flip_h = false
		$MoveSound.play()

	elif Input.is_action_just_pressed("ui_left") and position.x != 190 and alive == true:
		motion.x -= move_speed
		$Sprite.flip_h = true
		$MoveSound.play()
	else: 
		motion.x = 0
	
	
	# audio manager thingy
	if Input.is_action_just_pressed("ui_accept") and start_sound_played == false:
		$StartSound.play()
		start_sound_played = true

	
	collision = move_and_collide(motion)
	if collision:
		print(collision.collider.name )
		if collision.collider.is_in_group("Ghosts"):
			die()

func die():
	$CollisionShape2D.disabled = true
	$DeathSound.play()
	$AnimationPlayer.stop()
	$Busted.show()
	alive = false
	$Explosion.emitting = true



func _on_Win_Area_body_entered(body):
	print("winstart_sound_played")
