extends KinematicBody2D

const MOVE_SPEED = 150
var DELTA
var motion: Vector2
var started = false

func _ready():
	$AnimationPlayer.play("idle")
	$Indicator.global_transform.origin.x = 760 - 10
	Global.connect("start", self, "restart")
func _process(delta):
	if Input.is_action_pressed("ui_accept") and started == false:
		$AnimationPlayer.play("invisible")
		move(delta)
	if global_position.y > 864:
		$AnimationPlayer.play("visible")
	
	var collsion = move_and_collide(motion)
	if collsion:
		if collsion.collider.name == "Player":
			$Sprite.modulate.a8 = 255
			Global.game_over()
			motion.y -= MOVE_SPEED * delta

func move(delta):
	motion.y += MOVE_SPEED * delta
	started = true

func restart():
	get_tree().reload_current_scene()

