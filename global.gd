extends Node


signal dead
signal start

func game_over():
	emit_signal("dead")

func game_start():
	emit_signal("start")
