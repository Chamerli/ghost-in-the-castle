extends Node2D

func _ready():
	$AnimationPlayer.play("showcasing")
	$Camera2D.current = true

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") and $Player.alive == true:
		$AnimationPlayer.stop()
		$Camera2D.position = Vector2(760 / 2, 528)
		$Camera2D/ReadyText.hide()


func win():
	$WinSound.play()
	$Camera2D/WinText.show()


func _on_Win_Area_body_entered(body):
	if body.name == "Player":
		win()
		$Timer.start()


func _on_Timer_timeout():
	print(Sigleton.current_level)
	if Sigleton.current_level < 6:
		Sigleton.load_next_level()
	if Sigleton.current_level == 6:
		$Camera2D/WinText.text = "Your time:" + str(TimeHolder.m) + ':' + str(TimeHolder.s) + ':' + str(TimeHolder.ms)
