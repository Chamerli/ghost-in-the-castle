# Ghost in the castle


You are a *knight* wandering in a **big tower** known for its *ghosts* and *spirits* living in it, the ghosts are very tacky creatures, they just keep wandering with no real purpose, but when they see the sight of you, they hide by becoming invisible and charge at you, the little contact with them can make you choke to death, so the goal of the game is to remember the patterns and positions of the ghost before they notice you, and when you start they charge at you, so you have to ***dodge*** them when they arrive, thankfully, there's a little *red arrow* in the right of the screen that helps you keep track of the current distance between you and them

## Controls:

##### **Movement:** *Right_Arrow*, *Left_Arro* or *A*, *D*

##### **Starting:** *Entre*, *Space*


## The assets used for this game:

[The Tilesets](https://pixelfrog-assets.itch.io/kings-and-pigs)

[The Player and the Ghosts](https://broke-one-pixel-studio.itch.io/ogreghostskeletonandtauruspixelartsprites)